const dashboard = require('@userappstore/dashboard')

module.exports = {
  get: async (req) => {
    if (!req.query || !req.query.organizationid) {
      throw new Error('invalid-organizationid')
    }
    return dashboard.RedisList.count(`organization:invitations:${req.query.organizationid}`)
  }
}
