const dashboard = require('@userappstore/dashboard')
const orgs = require('../../../../../index.js')

module.exports = {
  lock: true,
  before: async (req) => {
    if (!req.query || !req.query.membershipid) {
      throw new Error('invalid-membershipid')
    }
    const membership = await orgs.Membership.load(req.query.membershipid)
    if (!membership) {
      throw new Error('invalid-membershipid')
    }
    req.query.organizationid = membership.organizationid
    const organization = await global.api.user.organizations.Organization.get(req)
    if (!organization) {
      throw new Error('invalid-organizationid')
    }
    req.organization = organization
  },
  delete: async (req) => {
    await orgs.Membership.deleteMembership(req.query.membershipid)
    await dashboard.RedisList.remove(`memberships`, req.query.membershipid)
    await dashboard.RedisList.remove(`account:memberships:${req.account.accountid}`, req.query.membershipid)
    await dashboard.RedisList.remove(`organization:memberships:${req.organization.organizationid}`, req.query.membershipid)
    req.success = true
  }
}
