const dashboard = require('@userappstore/dashboard')
const orgs = require('../../../../../index.js')

module.exports = {
  lock: true,
  before: async (req) => {
    if (!req.query || !req.query.organizationid) {
      throw new Error('invalid-organizationid')
    }
    const organization = await global.api.user.organizations.Organization.get(req)
    if (!organization) {
      throw new Error('invalid-organizationid')
    }
    if (organization.ownerid !== req.account.accountid) {
      throw new Error('invalid-account')
    }
  },
  delete: async (req) => {
    await orgs.Organization.deleteOrganization(req.query.organizationid)
    await dashboard.RedisList.remove(`organizations`, req.query.organizationid)
    await dashboard.RedisList.remove(`account:organizations:${req.account.accountid}`, req.query.organizationid)
    req.success = true
  }
}
