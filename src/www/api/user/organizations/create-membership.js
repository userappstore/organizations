const dashboard = require('@userappstore/dashboard')
const orgs = require('../../../../../index.js')

module.exports = {
  lock: true,
  before: async (req) => {
    if (!req.query || !req.query.invitationid) {
      throw new Error('invalid-invitationid')
    }
    if (!req.body || !req.body.code) {
      throw new Error('invalid-invitation-code')
    }
    if (global.minimumInvitationCodeLength > req.body.code.length ||
      global.maximumInvitationCodeLength < req.body.code.length) {
      throw new Error('invalid-invitation-code-length')
    }
    if (!req.body.name || !req.body.name.length) {
      throw new Error('invalid-membership-name')
    }
    if (global.minimumMembershipNameLength > req.body.name.length ||
      global.maximumMembershipNameLength < req.body.name.length) {
      throw new Error('invalid-membership-name-length')
    }
    if (!req.body.email || !req.body.email.length) {
      throw new Error('invalid-membership-email')
    }
    const invitation = await orgs.Invitation.load(req.query.invitationid)
    if (!invitation || (invitation.accepted && invitation.accepted !== req.account.accountid)) {
      throw new Error('invalid-invitation')
    }
    const organization = await orgs.Organization.load(invitation.organizationid)
    if (!organization) {
      throw new Error('invalid-organizationid')
    }
    if (organization.ownerid === req.account.accountid) {
      throw new Error('invalid-account')
    }
    const member = await orgs.Membership.isMember(organization.organizationid, req.account.accountid)
    if (member) {
      throw new Error('invalid-account')
    }
    req.data = { organization }
  },
  post: async (req) => {
    await orgs.Invitation.accept(req.data.organization.organizationid, req.body.code, req.account.accountid)
    const membership = await orgs.Membership.create(req.data.organization.organizationid, req.account.accountid)
    await dashboard.RedisObject.setProperties(membership.membershipid, {
      name: req.body.name,
      email: req.body.email,
      ip: req.ip,
      userAgent: req.userAgent,
      invitationid: req.query.invitationid
    })
    await dashboard.RedisObject.setProperty(req.query.invitationid, 'membershipid', membership.membershipid)
    await dashboard.RedisList.add(`memberships`, membership.membershipid)
    await dashboard.RedisList.add(`account:memberships:${req.account.accountid}`, membership.membershipid)
    await dashboard.RedisList.add(`account:organizations:${req.account.accountid}`, req.data.organization.organizationid)
    await dashboard.RedisList.add(`account:invitations:${req.account.accountid}`, req.query.invitationid)
    await dashboard.RedisList.add(`organization:memberships:${req.data.organization.organizationid}`, membership.membershipid)
    req.success = true
    return membership
  }
}
