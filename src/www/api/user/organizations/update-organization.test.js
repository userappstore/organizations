/* eslint-env mocha */
const assert = require('assert')
const TestHelper = require('../../../../../test-helper.js')

describe('/api/user/organizations/update-organization', async () => {
  it('should reject missing name', async () => {
    const owner = await TestHelper.createUser()
    await TestHelper.createOrganization(owner, { email: owner.profile.email, name: 'My organization' })
    const req = TestHelper.createRequest(`/api/user/organizations/update-organization?organizationid=${owner.organization.organizationid}`, 'PATCH')
    req.account = owner.account
    req.session = owner.session
    req.body = {
      name: '',
      email: owner.profile.email
    }
    let errorMessage
    try {
      await req.route.api.patch(req)
    } catch (error) {
      errorMessage = error.message
    }
    assert.strictEqual(errorMessage, 'invalid-organization-name')
  })

  it('should enforce name length', async () => {
    const owner = await TestHelper.createUser()
    await TestHelper.createOrganization(owner, { email: owner.profile.email, name: 'My organization' })
    const req = TestHelper.createRequest(`/api/user/organizations/update-organization?organizationid=${owner.organization.organizationid}`, 'PATCH')
    req.account = owner.account
    req.session = owner.session
    req.body = {
      name: '12345',
      email: owner.profile.email
    }
    global.minimumOrganizationNameLength = 100
    let errorMessage
    try {
      await req.route.api.patch(req)
    } catch (error) {
      errorMessage = error.message
    }
    assert.strictEqual(errorMessage, 'invalid-organization-name-length')
    global.maximumOrganizationNameLength = 1
    errorMessage = null
    try {
      await req.route.api.patch(req)
    } catch (error) {
      errorMessage = error.message
    }
    assert.strictEqual(errorMessage, 'invalid-organization-name-length')
  })

  it('should reject missing email', async () => {
    const owner = await TestHelper.createUser()
    await TestHelper.createOrganization(owner, { email: owner.profile.email, name: 'My organization' })
    const req = TestHelper.createRequest(`/api/user/organizations/update-organization?organizationid=${owner.organization.organizationid}`, 'PATCH')
    req.account = owner.account
    req.session = owner.session
    req.body = {
      name: owner.profile.firstName,
      email: null
    }
    let errorMessage
    try {
      await req.route.api.patch(req)
    } catch (error) {
      errorMessage = error.message
    }
    assert.strictEqual(errorMessage, 'invalid-organization-email')
  })

  it('should apply new values', async () => {
    const owner = await TestHelper.createUser()
    await TestHelper.createOrganization(owner, { email: owner.profile.email, name: 'My organization' })
    const req = TestHelper.createRequest(`/api/user/organizations/update-organization?organizationid=${owner.organization.organizationid}`, 'PATCH')
    req.account = owner.account
    req.session = owner.session
    req.body = {
      name: 'Organization Name',
      email: 'test@test.com'
    }
    await req.route.api.patch(req)
    req.session = await TestHelper.unlockSession(owner)
    const organizationNow = await req.route.api.patch(req)
    assert.strictEqual(organizationNow.name, 'Organization Name')
    assert.strictEqual(organizationNow.email, 'test@test.com')
  })
})
