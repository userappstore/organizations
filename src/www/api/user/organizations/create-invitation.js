const dashboard = require('@userappstore/dashboard')
const orgs = require('../../../../../index.js')

module.exports = {
  lock: true,
  before: async (req) => {
    if (!req.query || !req.query.organizationid) {
      throw new Error('invalid-organizationid')
    }
    if (req.body && req.body.codeHash) {
      return
    }
    if (!req.body || !req.body.code) {
      throw new Error('invalid-invitation-code')
    }
    if (global.minimumInvitationCodeLength > req.body.code.length ||
      global.maximumInvitationCodeLength < req.body.code.length) {
      throw new Error('invalid-invitation-code-length')
    }
    const organization = await global.api.user.organizations.Organization.get(req)
    if (!organization) {
      throw new Error('invalid-organizationid')
    }
    if (organization.ownerid !== req.account.accountid) {
      throw new Error('invalid-account')
    }
    req.body.codeHash = await dashboard.Hash.fixedSaltHash(req.body.code)
    delete (req.body.code)
  },
  post: async (req) => {
    const invitation = await orgs.Invitation.create(req.query.organizationid, req.body.codeHash)
    await dashboard.RedisObject.setProperty(invitation.invitationid, 'ip', req.ip)
    await dashboard.RedisObject.setProperty(invitation.invitationid, 'userAgent', req.userAgent)
    await dashboard.RedisList.add(`invitations`, invitation.invitationid)
    await dashboard.RedisList.add(`account:invitations:${req.account.accountid}`, invitation.invitationid)
    await dashboard.RedisList.add(`organization:invitations:${req.query.organizationid}`, invitation.invitationid)
    req.success = true
    return invitation
  }
}
