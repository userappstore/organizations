const dashboard = require('@userappstore/dashboard')
const orgs = require('../../../../../index.js')

module.exports = {
  lock: true,
  before: async (req) => {
    if (!req.query || !req.query.invitationid) {
      throw new Error('invalid-invitationid')
    }
    const invitation = await orgs.Invitation.load(req.query.invitationid)
    if (!invitation) {
      throw new Error('invalid-invitationid')
    }
    if (invitation.accepted) {
      throw new Error('invalid-invitation')
    }
    req.query.organizationid = invitation.organizationid
    const organization = await global.api.user.organizations.Organization.get(req)
    if (!organization) {
      throw new Error('invalid-organizationid')
    }
    if (organization.ownerid !== req.account.accountid) {
      throw new Error('invalid-account')
    }
    req.organization = organization
  },
  delete: async (req) => {
    await orgs.Invitation.deleteInvitation(req.query.invitationid)
    await dashboard.RedisList.remove(`invitations`, req.query.invitationid)
    await dashboard.RedisList.remove(`account:invitations:${req.account.accountid}`, req.query.invitationid)
    await dashboard.RedisList.remove(`organization:invitations:${req.organization.organizationid}`, req.query.invitationid)
    req.success = true
  }
}
