const dashboard = require('@userappstore/dashboard')
const orgs = require('../../../../../index.js')

module.exports = {
  lock: true,
  before: async (req) => {
    if (!req.query || !req.query.accountid) {
      throw new Error('invalid-accountid')
    }
    if (req.query.accountid !== req.account.accountid) {
      throw new Error('invalid-account')
    }
    if (!req.body || !req.body.name || !req.body.name.length) {
      throw new Error('invalid-organization-name')
    }
    if (global.minimumOrganizationNameLength > req.body.name.length ||
      global.maximumOrganizationNameLength < req.body.name.length) {
      throw new Error('invalid-organization-name-length')
    }
    if (!req.body.email || !req.body.email.length) {
      throw new Error('invalid-organization-email')
    }
  },
  post: async (req) => {
    const organization = await orgs.Organization.create(req.account.accountid, req.body.name)
    await dashboard.RedisObject.setProperties(organization.organizationid, {
      name: req.body.name,
      email: req.body.email,
      ip: req.ip,
      userAgent: req.userAgent
    })
    const profile = req.account.profileid ? await dashboard.Profile.load(req.account.profileid) : null
    const membershipInfo = { 
      name: profile ? `${profile.firstName} ${profile.lastName.substring(0, 1)}` : req.body.name,
      email: req.body.email,
      ip: req.ip,
      userAgent: req.userAgent
    }
    const membership = await orgs.Membership.create(organization.organizationid, req.account.accountid)
    await dashboard.RedisObject.setProperties(membership.membershipid, membershipInfo)
    await dashboard.RedisList.add(`organizations`, organization.organizationid)
    await dashboard.RedisList.add(`account:organizations:${req.account.accountid}`, organization.organizationid)
    req.success = true
    return organization
  }
}
