/* eslint-env mocha */
const assert = require('assert')
const TestHelper = require('../../../../test-helper.js')

describe(`/account/organizations/create-organization`, async () => {
  describe('CreateOrganization#GET', () => {
    it('should present the form', async () => {
      const owner = await TestHelper.createUser()
      const req = TestHelper.createRequest(`/account/organizations/create-organization`, 'GET')
      req.account = owner.account
      req.session = owner.session
      const res = TestHelper.createResponse()
      res.end = async (str) => {
        const doc = TestHelper.extractDoc(str)
        assert.strictEqual(doc.getElementById('submit-form').tag, 'form')
        assert.strictEqual(doc.getElementById('submit-button').tag, 'button')
      }
      return req.route.api.get(req, res)
    })
  })

  describe('CreateOrganization#POST', () => {
    it('should reject missing name', async () => {
      const user = await TestHelper.createUser()
      const req = TestHelper.createRequest('/account/organizations/create-organization', 'POST')
      req.account = user.account
      req.session = user.session
      req.body = {
        name: ''
      }
      const res = TestHelper.createResponse()
      res.end = async (str) => {
        const doc = TestHelper.extractDoc(str)
        const message = doc.getElementById('message-container').child[0]
        assert.strictEqual(message.attr.template, 'invalid-organization-name')
      }
      return req.route.api.post(req, res)
    })

    it('should enforce name length', async () => {
      const user = await TestHelper.createUser()
      const req = TestHelper.createRequest('/account/organizations/create-organization', 'POST')
      req.account = user.account
      req.session = user.session
      req.body = {
        name: '1'
      }
      global.minimumOrganizationNameLength = 2
      const res = TestHelper.createResponse()
      res.end = async (str) => {
        const doc = TestHelper.extractDoc(str)
        const message = doc.getElementById('message-container').child[0]
        assert.strictEqual(message.attr.template, 'invalid-organization-name-length')
        // too long
        const req2 = TestHelper.createRequest('/account/organizations/create-organization', 'POST')
        req2.account = user.account
        req2.session = user.session
        req2.body = {
          name: '1234567890'
        }
        const res2 = TestHelper.createResponse()
        res2.end = async (str) => {
          const doc = TestHelper.extractDoc(str)
          const message = doc.getElementById('message-container').child[0]
          assert.strictEqual(message.attr.template, 'invalid-organization-name-length')
        }
        global.maximumOrganizationNameLength = 1
        return req.route.api.post(req, res2)
      }
      return req.route.api.post(req, res)
    })

    it('should reject missing email', async () => {
      const user = await TestHelper.createUser()
      const req = TestHelper.createRequest('/account/organizations/create-organization', 'POST')
      req.session = user.session
      req.account = user.account
      req.body = {
        name: 'org-name',
        email: null
      }
      const res = TestHelper.createResponse()
      res.end = async (str) => {
        const doc = TestHelper.extractDoc(str)
        const message = doc.getElementById('message-container').child[0]
        assert.strictEqual(message.attr.template, 'invalid-organization-email')
      }
      return req.route.api.post(req, res)
    })

    it('should create organization', async () => {
      const owner = await TestHelper.createUser()
      const req = TestHelper.createRequest(`/account/organizations/create-organization`, 'POST')
      req.account = owner.account
      req.session = owner.session
      req.body = {
        name: 'org-name',
        email: 'test@test.com'
      }
      const res = TestHelper.createResponse()
      res.end = async (str) => {
        req.session = await TestHelper.unlockSession(owner)
        const res2 = TestHelper.createResponse()
        res2.end = async (str) => {
          const doc = TestHelper.extractDoc(str)
          const messageContainer = doc.getElementById('message-container')
          const message = messageContainer.child[0]
          assert.strictEqual(message.attr.template, 'success')
        }
        return req.route.api.get(req, res2)
      }
      return req.route.api.post(req, res)
    })
  })
})
