/* eslint-env mocha */
global.applicationPath = __dirname
const dashboard = require('@userappstore/dashboard')
const TestHelper = module.exports = dashboard.loadTestHelper()

module.exports.acceptInvitation = acceptInvitation
module.exports.createInvitation = createInvitation
module.exports.createMembership = createMembership
module.exports.createOrganization = createOrganization

beforeEach((callback) => {
  global.minimumOrganizationNameLength = 1
  global.maximumOrganizationNameLength = 100
  global.minimumMembershipNameLength = 1
  global.maximumMembershipNameLength = 100
  global.minimumInvitationCodeLength = 1
  global.maximumInvitationCodeLength = 100
  return callback()
})

async function createOrganization (user, organization) {
  const req = TestHelper.createRequest(`/api/user/organizations/create-organization?accountid=${user.account.accountid}`, 'POST')
  req.account = user.account
  req.session = user.session
  req.body = {
    name: organization.name,
    email: organization.email
  }
  await req.route.api.post(req)
  req.session = await TestHelper.unlockSession(user)
  user.organization = await req.route.api.post(req)
  user.session = await dashboard.Session.load(user.session.sessionid)
  return user.organization
}

async function createInvitation (owner) {
  const code = 'invitation-' + new Date().getTime() + '-' + Math.ceil(Math.random() * 1000)
  const req = TestHelper.createRequest(`/api/user/organizations/create-invitation?organizationid=${owner.organization.organizationid}`, 'POST')
  req.account = owner.account
  req.session = owner.session
  req.body = { code }
  await req.route.api.post(req)
  req.session = await TestHelper.unlockSession(owner)
  owner.invitation = await req.route.api.post(req)
  owner.invitation.code = code
  owner.session = await dashboard.Session.load(owner.session.sessionid)
  return owner.invitation
}

async function acceptInvitation (user, owner) {
  const req = TestHelper.createRequest(`/api/user/organizations/create-membership?invitationid=${owner.invitation.invitationid}`, 'POST')
  req.account = user.account
  req.session = user.session
  req.body = { code: owner.invitation.code, name: 'Person', email: user.profile.email }
  await req.route.api.post(req)
  req.session = await TestHelper.unlockSession(user)
  user.membership = await req.route.api.post(req)
  user.session = await dashboard.Session.load(user.session.sessionid)
  return user.membership
}

async function createMembership (user, owner) {
  await createInvitation(owner)
  return acceptInvitation(user, owner)
}
